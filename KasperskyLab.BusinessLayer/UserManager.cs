using System;
using System.Collections.Generic;
using System.Linq;
using KasperskyLab.LowLevel.DataAccess;
using KasperskyLab.LowLevel.Model.ForDatabase;

namespace KasperskyLab.BusinessLayer
{
    public class UserManager : IUserManager
    {
        private readonly IRepository _repository;

        public UserManager(IRepository repository)
        {
            _repository = repository;
        }

        public bool AddOrCreate(User user)
        {
            if (_repository.Get<User>().SingleOrDefault(x => x.UserId == user.UserId) == null)
                return _repository.Insert(user);

            return _repository.Update(user);
        }

        public User Get(Guid userId)
        {
            return _repository.Get<User>().SingleOrDefault(x => x.UserId == userId);
        }

        public List<User> GetAll()
        {
            return _repository.Get<User>().ToList();
        }
    }
}