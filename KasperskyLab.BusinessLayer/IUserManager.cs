﻿using System;
using System.Collections.Generic;
using KasperskyLab.LowLevel.Model.ForDatabase;

namespace KasperskyLab.BusinessLayer
{
    public interface IUserManager
    {
        bool AddOrCreate(User user);

        User Get(Guid userId);

        List<User> GetAll();
    }
}
