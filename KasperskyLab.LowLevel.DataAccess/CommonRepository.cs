﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using KasperskyLab.LowLevel.Model.ForDatabase;

namespace KasperskyLab.LowLevel.DataAccess
{
    public class CommonRepository : IRepository
    {
        private DbContext _dbContext;


        public CommonRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<TEntity> Get<TEntity>() where TEntity : IdentifyEntity
        {
            return _dbContext.Set<TEntity>();
        }

        public bool Insert<TEntity>(TEntity entity) where TEntity : IdentifyEntity
        {
            _dbContext.Set<TEntity>().Add(entity);
            if (_dbContext.SaveChanges() != 1)
                return false;

            return true;
        }

        public bool InsertMany<TEntity>(IList<TEntity> entities) where TEntity : IdentifyEntity
        {
            throw new NotImplementedException();
        }

        public bool Update<TEntity>(TEntity entity) where TEntity : IdentifyEntity
        {
            _dbContext.Set<TEntity>().AddOrUpdate(entity);
            return _dbContext.SaveChanges() == 1;
        }

        public bool UpdateMany<TEntity>(IList<TEntity> entities) where TEntity : IdentifyEntity
        {
            throw new NotImplementedException();
        }

        public bool Remove<TEntity>(TEntity entity) where TEntity : IdentifyEntity
        {
            throw new NotImplementedException();
        }

        public bool RemoveMany<TEntity>(IEnumerable<TEntity> entities) where TEntity : IdentifyEntity
        {
            throw new NotImplementedException();
        }
    }
}
