﻿using System.Collections.Generic;
using KasperskyLab.LowLevel.Model.ForDatabase;

namespace KasperskyLab.LowLevel.DataAccess
{
    public interface IRepository
    {
        IEnumerable<TEntity> Get<TEntity>() where TEntity : IdentifyEntity;


        bool Insert<TEntity>(TEntity entity) where TEntity : IdentifyEntity;


        bool InsertMany<TEntity>(IList<TEntity> entities) where TEntity : IdentifyEntity;


        bool Update<TEntity>(TEntity entity) where TEntity : IdentifyEntity;


        bool UpdateMany<TEntity>(IList<TEntity> entities) where TEntity : IdentifyEntity;


        bool Remove<TEntity>(TEntity entity) where TEntity : IdentifyEntity;


        bool RemoveMany<TEntity>(IEnumerable<TEntity> entities) where TEntity : IdentifyEntity;
    }
}
