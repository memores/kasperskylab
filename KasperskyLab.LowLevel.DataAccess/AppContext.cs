﻿using System.Data.Entity;
using KasperskyLab.LowLevel.Model.ForDatabase;

namespace KasperskyLab.LowLevel.DataAccess
{
    public class AppContext : DbContext
    {
        public DbSet<User> Users { get; set; }
    }
}
