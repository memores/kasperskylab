using System;
using System.Configuration;
using System.Data.Entity;
using System.Web;
using System.Web.Http;
using KasperskyLab.BusinessLayer;
using KasperskyLab.LowLevel.DataAccess;
using KasperskyLab.LowLevel.Logging;
using KasperskyLab.Services.UserInfoImporter;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;
using Serilog;
using ILogger = KasperskyLab.LowLevel.Logging.ILogger;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(NinjectWebCommon), "Stop")]

namespace KasperskyLab.Services.UserInfoImporter
{
    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                GlobalConfiguration.Configuration.DependencyResolver = new NinjectResolver(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }

            
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<ILogger>().To<Logger>().InSingletonScope();

            kernel.Bind<LoggerConfiguration>().ToMethod(context =>
            {
                var configuration = new LoggerConfiguration();
                configuration.WriteTo.RollingFile(ConfigurationManager.AppSettings["LogFilePath"],
                    outputTemplate: ConfigurationManager.AppSettings["LogTraceTemplate"]);
                return configuration;
            });
            kernel.Bind<IUserManager>().To<UserManager>();
            kernel.Bind<IRepository>().To<CommonRepository>().WhenInjectedInto<IUserManager>();
            kernel.Bind<DbContext>().To<AppContext>().WhenInjectedInto<IRepository>().InThreadScope();
        }        
    }
}
