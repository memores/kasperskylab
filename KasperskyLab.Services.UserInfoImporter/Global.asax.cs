﻿using System.Web.Http;

namespace KasperskyLab.Services.UserInfoImporter
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
