﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web.Http;
using KasperskyLab.BusinessLayer;
using KasperskyLab.LowLevel.Helpers;
using KasperskyLab.LowLevel.Logging;
using KasperskyLab.LowLevel.Model.ForDatabase;
using KasperskyLab.LowLevel.Model.ForServices;

namespace KasperskyLab.Services.UserInfoImporter.Controllers
{
    public class UserController : ApiController, IUserController
    {
        private readonly IUserManager _userManager;
        private ILogger _logger;

        public UserController(IUserManager userManager, ILogger logger)
        {
            _userManager = userManager;
            _logger = logger;
        }

        [HttpGet, Route("api/getAllUsers")]
        public IEnumerable<UserInfo> GetAllUsers()
        {
            var users = _userManager.GetAll();
            List<UserInfo> usersInfo = new List<UserInfo>();
            foreach (var user in users)
            {
                usersInfo.Add(user.To<UserInfo>());
            }
            return usersInfo;
        }

      
        [HttpPost, Route("import.json")]
        public bool PostUser(SyncProfileRequest request)
        {
            _logger.Log(TraceEventType.Information, request.ToReflectedString());
            try
            {
                var result = _userManager.AddOrCreate(request.To<User>());
                _logger.Log(TraceEventType.Information, $"The result for {request.RequestId} is {result}");
                return result;
            }
            catch (Exception e)
            {
                _logger.Log(TraceEventType.Error, $"The request with {request.RequestId} can`t be invoked\n\n{e}");
                return false;
            }
        }
    }
}
