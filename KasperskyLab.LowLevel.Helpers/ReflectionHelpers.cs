﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace KasperskyLab.LowLevel.Helpers
{
    public static class ReflectionHelpers
    {
        public static string ToReflectedString(this object obj)
        {
            var properties = obj.GetType().GetProperties();
            List<string> propertiesInfo = new List<string>();
            
            foreach (var property in properties)
            {
                propertiesInfo.Add($"{property.Name} : {property.GetValue(obj)}");
            }
            return string.Join("\t", propertiesInfo);
        }

        public static TType To<TType>(this object inputObj)
        {
            var outputType = typeof (TType);
            var outputObj = Activator.CreateInstance(outputType);
            var outputProperties = outputObj.GetType().GetProperties().ToDictionary(x=>x.Name, x=>x);

            var inputProperties = inputObj.GetType().GetProperties();
            foreach (var inputProperty in inputProperties)
            {
                PropertyInfo outputProperty;
                if (!outputProperties.TryGetValue(inputProperty.Name, out outputProperty))
                    continue;

                object value;
                //var pka = outputProperty.CustomAttributes.SingleOrDefault(x => x.AttributeType == typeof(PrimaryKeyAttribute));
                //if (pka != null)
                //{
                //    var dbPkProperty = typeof(PrimaryKeyAttribute).GetProperties().SingleOrDefault(x => x.Name == "PrimaryDbKey");
                //    value = dbPkProperty.GetValue(pka);
                //    outputProperty.SetValue(outputObj, value);
                //}
                
                value = inputProperty.GetValue(inputObj);
                outputProperty.SetValue(outputObj, value);
            }

            return (TType) outputObj;
        }
    }
}
