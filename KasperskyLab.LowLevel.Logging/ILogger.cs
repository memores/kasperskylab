﻿using System;
using System.Diagnostics;

namespace KasperskyLab.LowLevel.Logging
{
    public interface ILogger
    {
        void Log(TraceEventType eventLevel, string eventMessage, Exception exception = null);
    }
}
