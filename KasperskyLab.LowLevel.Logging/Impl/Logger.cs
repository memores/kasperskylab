﻿using System;
using System.Diagnostics;
using Serilog;
using Serilog.Events;

namespace KasperskyLab.LowLevel.Logging
{
    public class Logger : ILogger
    {
        public Logger(LoggerConfiguration configuration){
            Serilog.Log.Logger = configuration.CreateLogger();
        }

        public void Log(TraceEventType eventLevel, string eventMessage, Exception exception = null)
        {
            LogEventLevel eventType;
            if (!Enum.TryParse(eventLevel.ToString(), out eventType))
            {
                Serilog.Log.Write(LogEventLevel.Information,
                    $"{eventMessage}\n\nCan`t write log\n\nCan`t find LogEventLevel by {eventLevel}");
                return;
            }

            if (exception == null)
            {
                Serilog.Log.Write(eventType, eventMessage);
                return;
            }

            Serilog.Log.Write(eventType, exception, eventMessage);
        }
    }
}
