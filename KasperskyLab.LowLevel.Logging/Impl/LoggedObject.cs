﻿namespace KasperskyLab.LowLevel.Logging
{
    public class LoggedObject : ILoggedObject
    {
        protected ILogger _logger;


        public ILogger Logger
        {
            get { return _logger; }
            set { _logger = value; }
        }
    }
}