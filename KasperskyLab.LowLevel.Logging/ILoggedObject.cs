﻿namespace KasperskyLab.LowLevel.Logging
{
    public interface ILoggedObject
    {
        ILogger Logger { get; set; }
    }
}