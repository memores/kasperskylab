﻿using System;
using System.Diagnostics;
using System.Reflection;
using PostSharp.Aspects;

namespace KasperskyLab.LowLevel.Logging
{
    [Serializable]
    public class TraceAttribute : OnMethodBoundaryAspect
    {
        public ILogger Logger;

        [NonSerialized]
        private string _enteringMessage;
        [NonSerialized]
        private string _exitingMessage;


        public override void RuntimeInitialize(MethodBase method)
        {
            string methodName = method.DeclaringType?.FullName + method.Name;
            _enteringMessage = "Entering " + methodName;
            _exitingMessage = "Exiting " + methodName;
        }

        public override void OnEntry(MethodExecutionArgs args)
        {
            Logger = GetLogger(args);
            Logger.Log(TraceEventType.Information, _enteringMessage);
        }

        public override void OnException(MethodExecutionArgs args)
        {
            Logger.Log(TraceEventType.Error, $"{args.Arguments}", args.Exception);
        }

        public override void OnExit(MethodExecutionArgs args)
        {
            Logger.Log(TraceEventType.Information, _exitingMessage);
        }


        private ILogger GetLogger(MethodExecutionArgs args)
        {
            var obj = (ILoggedObject) args.Instance;
            return obj?.Logger;
        }
    }
}