﻿using System;

namespace KasperskyLab.LowLevel.Model.ForDatabase
{
    public class User : IdentifyEntity
    {
        public Guid UserId { get; set; }


        public bool? AdvertisingOptIn { get; set; }

       
        public string CountryIsoCode { get; set; }

       
        public DateTime DateModified { get; set; }

       
        public string Locale { get; set; }
    }

   
}