﻿using System;

namespace KasperskyLab.LowLevel.Model.ForDatabase
{
    public class IdentifyEntity
    {
        public Guid Id { get; set; }

        public IdentifyEntity()
        {
            Id = Guid.NewGuid();
        }
    }
}