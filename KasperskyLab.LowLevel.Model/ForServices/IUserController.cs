﻿using System.Collections.Generic;

namespace KasperskyLab.LowLevel.Model.ForServices
{
    public interface IUserController
    {
        IEnumerable<UserInfo> GetAllUsers();

        bool PostUser(SyncProfileRequest request);
    }
}