﻿using System;
using KasperskyLab.LowLevel.Model.ForServices.Base;

namespace KasperskyLab.LowLevel.Model.ForServices
{
    public class SyncProfileRequest : MyAccountRequestBase
    {
        public bool? AdvertisingOptIn { get; set; }

        public string CountryIsoCode { get; set; }

        public DateTime DateModified { get; set; }

        public string Locale { get; set; }
    }
}