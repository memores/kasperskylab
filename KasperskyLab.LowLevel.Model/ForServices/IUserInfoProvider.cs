﻿using System;
using System.ServiceModel;

namespace KasperskyLab.LowLevel.Model.ForServices
{
    [ServiceContract]
    public interface IUserInfoProvider
    {
        [OperationContract]
        [FaultContract(typeof(UserNotFound))]
        UserInfo GetUserInfo(Guid userId);
    }
}