using System;

namespace KasperskyLab.LowLevel.Model.ForServices.Base
{
    public abstract class MyAccountRequestBase
    {
        public Guid UserId { get; set; }

        public Guid RequestId { get; set; }
    }
}