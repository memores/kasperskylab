﻿using System;
using System.Linq;
using Dccelerator;
using FakeItEasy;
using KasperskyLab.BusinessLayer;
using KasperskyLab.LowLevel.DataAccess;
using KasperskyLab.LowLevel.Logging;
using KasperskyLab.LowLevel.Model.ForDatabase;
using KasperskyLab.LowLevel.Model.ForServices;
using KasperskyLab.Services.UserInfoProvider;
using Ninject;
using Xbehave;
using Xunit;

namespace KasperskyLab.Tests
{
    public class When_using_user_info_provider
    {
        private UserInfo _userInfo;
        private Guid _guid;


        [Scenario]
        public void GettingUserInfo()
        {
            var kernel = new Kernel();
            var userInfoProvider = new UserInfoProvider(A.Fake<ILogger>(), kernel.Get<IUserManager>());
            var repository = kernel.Get<IRepository>();

            _guid = repository.Get<User>().Shuffle().FirstOrDefault().UserId;

            "GetUserInfo should be invoked".x(() => _userInfo = userInfoProvider.GetUserInfo(_guid));

            "Response should not be null".x(() => Assert.NotSame(_userInfo, null));

            "UserId should be equal request Id".x(() => Assert.Equal(_userInfo.UserId, _guid));
        }
    }
}
