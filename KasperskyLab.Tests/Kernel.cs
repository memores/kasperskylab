using KasperskyLab.LowLevel.Logging;
using KasperskyLab.Services.UserInfoProvider.NInjectServiceHostFactory.Modules;
using Ninject;

namespace KasperskyLab.Tests
{
    public class Kernel : StandardKernel
    {
        public Kernel() : base(new TraceModule(), new BusinessLayerModule(), new DataAccessNinjectModule())
        {
            //Bind<IUserInfoProvider>().To<UserInfoProvider>();
        }
    }
}