using System.Linq;
using Dccelerator;
using FakeItEasy;
using KasperskyLab.BusinessLayer;
using KasperskyLab.LowLevel.DataAccess;
using KasperskyLab.LowLevel.Helpers;
using KasperskyLab.LowLevel.Logging;
using KasperskyLab.LowLevel.Model.ForDatabase;
using KasperskyLab.LowLevel.Model.ForServices;
using KasperskyLab.Services.UserInfoImporter.Controllers;
using Ninject;
using Xbehave;
using Xunit;

namespace KasperskyLab.Tests
{
    public class UserInfoImporter_specs
    {
        private bool _response;
        private static SyncProfileRequest _request;

        [Scenario]
        public void AddNewUser()
        {
            var kernel = new Kernel();
            var userInfoImporter = new UserController(kernel.Get<IUserManager>(), A.Fake<ILogger>());
            var repository = kernel.Get<IRepository>();

            _request = RandomMaker.Make<SyncProfileRequest>(true);

            "GetUserInfo should be invoked".x(() => _response = userInfoImporter.PostUser(_request));

            "Response should be true".x(() => Assert.Equal(_response, true));

            "Current user shoud be inserted to db".x(() => Assert.NotSame(repository.Get<User>().SingleOrDefault(x => x.UserId == _request.UserId), null));

            "User from db shoud be like user from request".x(() =>
            {
                var userFromDb = repository.Get<User>().SingleOrDefault(x => x.UserId == _request.UserId);
                var userFromRequest = _request.To<User>();
                
                Assert.Equal(userFromDb.UserId, userFromRequest.UserId);
                Assert.Equal(userFromDb.AdvertisingOptIn, userFromRequest.AdvertisingOptIn);
                Assert.Equal(userFromDb.CountryIsoCode, userFromRequest.CountryIsoCode);
                Assert.Equal(userFromDb.Locale, userFromRequest.Locale);
            });
        }

        [Scenario]
        public void UpdateExistingUses()
        {
            var kernel = new Kernel();
            var userInfoImporter = new UserController(kernel.Get<IUserManager>(), A.Fake<ILogger>());
            var repository = kernel.Get<IRepository>();

            _request = RandomMaker.Make<SyncProfileRequest>();
            _request.UserId = repository.Get<User>().Shuffle().FirstOrDefault().UserId;

            "GetUserInfo should be invoked".x(() => _response = userInfoImporter.PostUser(_request));

            "Response should be true".x(() => Assert.Equal(_response, true));

            "Current user shoud be inserted to db".x(() => Assert.NotSame(repository.Get<User>().SingleOrDefault(x => x.UserId == _request.UserId), null));

            "User from db shoud be like user from request".x(() =>
            {
                var userFromDb = repository.Get<User>().SingleOrDefault(x => x.UserId == _request.UserId);
                var userFromRequest = _request.To<User>();

                Assert.Equal(userFromDb.UserId, userFromRequest.UserId);
                Assert.Equal(userFromDb.AdvertisingOptIn, userFromRequest.AdvertisingOptIn);
                Assert.Equal(userFromDb.CountryIsoCode, userFromRequest.CountryIsoCode);
                Assert.Equal(userFromDb.Locale, userFromRequest.Locale);
            });
        }
    }
}