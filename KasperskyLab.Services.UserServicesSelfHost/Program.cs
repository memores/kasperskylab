﻿using System;
using System.Web.Http;
using System.Web.Http.SelfHost;
using KasperskyLab.Services.UserInfoImporter;
using Ninject.Extensions.Wcf;
using Ninject.Extensions.Wcf.SelfHost;
using Ninject.Web.Common.SelfHost;

namespace KasperskyLab.Services.UserServicesSelfHost
{
    class Program
    {
        static void Main(string[] args)
        {
            var kernel = new Kernel();

            var userProvider = GetWcfConfiguration<UserInfoProvider.UserInfoProvider>();
            HttpSelfHostConfiguration webApiConfiguration = GetWebApiConfiguration(kernel);


            Console.WriteLine("Print 'exit', 'quit' or 'stop' if you want to stop services");
            using (var selfhostWebApi = new HttpSelfHostServer(webApiConfiguration))
            using (var selfhostWcf = new NinjectSelfHostBootstrapper(() => kernel, userProvider, webApiConfiguration))
            {
                selfhostWcf.Start();
                selfhostWebApi.OpenAsync().Wait();

                while (true)
                {
                    var line = Console.ReadLine();

                    if (string.Equals(line, "exit", StringComparison.InvariantCultureIgnoreCase)
                        || string.Equals(line, "quit", StringComparison.InvariantCultureIgnoreCase)
                        || string.Equals(line, "stop", StringComparison.InvariantCultureIgnoreCase))
                    {
                        selfhostWebApi.CloseAsync().Wait();
                        break;
                    }
                }
            }

        }

        private static NinjectWcfConfiguration GetWcfConfiguration<T>()
        {
            return NinjectWcfConfiguration.Create<T, NinjectServiceSelfHostFactory>();
        }

        private static HttpSelfHostConfiguration GetWebApiConfiguration(Kernel kernel)
        {
            var webApiConfiguration = new HttpSelfHostConfiguration("http://localhost:18092");

            webApiConfiguration.DependencyResolver = new NinjectResolver(kernel);
            webApiConfiguration.MapHttpAttributeRoutes();

            webApiConfiguration.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            return webApiConfiguration;
        }
    }
}
