﻿using KasperskyLab.LowLevel.Logging;
using KasperskyLab.Services.UserInfoProvider.NInjectServiceHostFactory.Modules;
using Ninject;

namespace KasperskyLab.Services.UserServicesSelfHost
{
    public class Kernel : StandardKernel
    {
        public Kernel() : base(new TraceModule(), new BusinessLayerModule(), new DataAccessNinjectModule())
        {
            
        }
    }
}