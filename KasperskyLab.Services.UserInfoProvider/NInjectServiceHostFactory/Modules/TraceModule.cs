using System.Configuration;
using KasperskyLab.LowLevel.Logging;
using Ninject.Modules;
using Serilog;
using ILogger = KasperskyLab.LowLevel.Logging.ILogger;

namespace KasperskyLab.Services.UserInfoProvider.NInjectServiceHostFactory.Modules
{
    public class TraceModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ILogger>().To<Logger>().InSingletonScope();

            Bind<LoggerConfiguration>().ToMethod(context =>
            {
                var configuration = new LoggerConfiguration();
                configuration.WriteTo.RollingFile(ConfigurationManager.AppSettings["LogFilePath"],
                    outputTemplate: ConfigurationManager.AppSettings["LogTraceTemplate"]);
                return configuration;
            });
        }
    }
}