﻿using KasperskyLab.BusinessLayer;
using Ninject.Modules;

namespace KasperskyLab.Services.UserInfoProvider.NInjectServiceHostFactory.Modules
{
    public class BusinessLayerModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserManager>().To<UserManager>();
        }
    }
}