﻿using System.Data.Entity;
using KasperskyLab.LowLevel.DataAccess;
using Ninject.Modules;

namespace KasperskyLab.Services.UserInfoProvider.NInjectServiceHostFactory.Modules
{
    public class DataAccessNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IRepository>().To<CommonRepository>();
            Bind<DbContext>().To<AppContext>().WhenInjectedInto<IRepository>();
        }
    }
}