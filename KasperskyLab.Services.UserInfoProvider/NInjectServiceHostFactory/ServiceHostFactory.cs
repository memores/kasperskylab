﻿using Ninject;
using Ninject.Extensions.Wcf;

namespace KasperskyLab.Services.UserInfoProvider.NInjectServiceHostFactory
{
    public class ServiceHostFactory : NinjectServiceHostFactory
    {
        private IKernel _kernelInstance;

        public IKernel KernelInstance => _kernelInstance ?? (_kernelInstance = new Kernel());

        public ServiceHostFactory()
        {
            SetKernel(KernelInstance);
        }
    }
}