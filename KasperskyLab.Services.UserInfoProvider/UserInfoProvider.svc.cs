﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using KasperskyLab.BusinessLayer;
using KasperskyLab.LowLevel.Helpers;
using KasperskyLab.LowLevel.Logging;
using KasperskyLab.LowLevel.Model.ForServices;

namespace KasperskyLab.Services.UserInfoProvider
{
    public class UserInfoProvider : LoggedObject, IUserInfoProvider
    {
        private readonly IUserManager _userManager;
        
        public UserInfoProvider(ILogger logger, IUserManager userManager)
        {
            _userManager = userManager;

            _logger = logger;
        }

        [Trace]
        public UserInfo GetUserInfo(Guid userId)
        {
            _logger.Log(TraceEventType.Information, $"Request: {userId}");

            var user = _userManager.Get(userId);

            if (user == null)
                throw new FaultException<UserNotFound>(new UserNotFound(), new FaultReason("Can`t find user by id {userId}"));

            var response = user.To<UserInfo>();
            _logger.Log(TraceEventType.Information, $"Response: {response.ToReflectedString()}");
            return response;
        }
    }
}
